import React from "react";
import s from './Header.module.css'
import { NavLink,} from "react-router-dom";
import About from "./Contact/About";

const Header = () => {
    return (
        <header className={s.header}>
            <img src="https://www.seekpng.com/png/full/205-2051834_support-americas-high-school-americas-high-school-el.png" alt="school" />
            <nav>
            <ul>
                
                <li>
                    <NavLink
                    to='/home'
                    >
                    Home
                    </NavLink>
                </li>
                <li>
                        <NavLink to='/About'>About</NavLink>
                    </li>
                <li>
                    <NavLink to='/settings'>
                    Settings
                    </NavLink>
                    </li>
            </ul>
            </nav>
        </header>
    )
}
export default Header