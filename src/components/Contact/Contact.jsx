import './Contact.css'

const ContactList = ({ contacts,}) => {
  return (
    <ul className="contact_list">
      {contacts.map(({ id, name, number }) => (
        <li key={id} className="contact_list_item">
          <p className="contact_name">
            {name}: {number}
          </p>
        </li>
      ))}
    </ul>
  );
};

export default ContactList;