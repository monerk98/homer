import './App.css';
import React, { Component } from 'react'
import Header from './components/Header';
import Contact from './components/Contact/Contact';
import {Switch, Route } from 'react-router-dom';

export default class App extends Component {
  state = {
    contacts: [
      { id: "id-1", name: "Rosie Simpson", number: "459-12-56" },
      { id: "id-2", name: "Hermione Kline", number: "443-89-12" },
      { id: "id-3", name: "Eden Clements", number: "645-17-79" },
      { id: "id-4", name: "Annie Copeland", number: "227-91-26" },
    ],
    filter: ''
  }

  getThis = () => {
    const {contacts} = this.state;
    return contacts.filter(contact => 
      contact.name.toLowerCase()
    )
  }
  render() {
    const Visible = this.getThis()
    return (
      <div>
        <Switch>
        <Route path="/">
        <Header/>
        </Route>
        </Switch>
        <Contact contacts={Visible}/>

      </div>
    )
  }
}

